import 'dart:convert';
import 'package:args/args.dart';

main(List<String> args) {
  final parser = ArgParser();
  final parsedArgs = parser.parse(args);

  if(parsedArgs.arguments.length < 1){
    print("Error : Usage => dart getClientCredential.dart clientName");
  } else {
    final clientID = parsedArgs.arguments[0];
    final String clientCredentials = const Base64Encoder().convert(
        "$clientID:".codeUnits).toString();

    print(clientCredentials);
  }
}