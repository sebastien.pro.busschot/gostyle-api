import 'dart:async';
import 'package:aqueduct/aqueduct.dart';   

class Migration7 extends Migration { 
  @override
  Future upgrade() async {
   		database.addColumn("_Product", SchemaColumn.relationship("file", ManagedPropertyType.bigInteger, relatedTableName: "_ProductFile", relatedColumnName: "fileId", rule: DeleteRule.nullify, isNullable: true, isUnique: true));
		database.deleteColumn("_ProductFile", "product");
  }
  
  @override
  Future downgrade() async {}
  
  @override
  Future seed() async {}
}
    