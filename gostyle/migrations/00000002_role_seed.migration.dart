import 'dart:async';
import 'package:aqueduct/aqueduct.dart';   

class Migration2 extends Migration { 
  @override
  Future upgrade() async {
   
  }
  
  @override
  Future downgrade() async {}
  
  @override
  Future seed() async {
    final List<Map<String, dynamic>> roles = [
      {
        "role": "user"
      },
      {
        "role": "admin"
      }
    ];

    for (final Map<String, dynamic> role in roles) {
      await database.store.execute(
          "INSERT INTO _Role (role) VALUES (@role)",
          substitutionValues: {
            "role": role["role"]
          }
      );
    }
  }
}