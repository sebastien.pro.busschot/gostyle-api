import 'dart:async';
import 'package:aqueduct/aqueduct.dart';   

class Migration6 extends Migration { 
  @override
  Future upgrade() async {
   		database.createTable(SchemaTable("_ProductFile", [SchemaColumn("fileId", ManagedPropertyType.bigInteger, isPrimaryKey: true, autoincrement: true, isIndexed: false, isNullable: false, isUnique: false),SchemaColumn("fileName", ManagedPropertyType.string, isPrimaryKey: false, autoincrement: false, isIndexed: false, isNullable: false, isUnique: true)]));
		database.addColumn("_ProductFile", SchemaColumn.relationship("product", ManagedPropertyType.bigInteger, relatedTableName: "_Product", relatedColumnName: "productId", rule: DeleteRule.nullify, isNullable: true, isUnique: true));
		database.deleteColumn("_Product", "imageUrl");
  }
  
  @override
  Future downgrade() async {}
  
  @override
  Future seed() async {}
}
    