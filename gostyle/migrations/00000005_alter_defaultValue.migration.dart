import 'dart:async';
import 'package:aqueduct/aqueduct.dart';   

class Migration5 extends Migration { 
  @override
  Future upgrade() async {
   		database.alterColumn("_User", "deleted", (c) {c.defaultValue = "false";});
		database.alterColumn("_Product", "deleted", (c) {c.defaultValue = "false";c.isNullable = true;});
		database.alterColumn("_Coupon", "percentageDiscount", (c) {c.isNullable = true;});
		database.alterColumn("_Coupon", "deleted", (c) {c.defaultValue = "false";});
  }
  
  @override
  Future downgrade() async {}
  
  @override
  Future seed() async {}
}
    