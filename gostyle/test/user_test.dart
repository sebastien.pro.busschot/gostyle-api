import 'dart:math';
import 'package:gostyle/model/city.dart';
import 'package:gostyle/model/user.dart';

import 'harness/app.dart';


Future main() async {
  const token = "1wiK3i64txDxiwB8UwvsJd7e0Hl89VkS";
  final harness = Harness()..install()..options.configurationFilePath = 'config.yaml';
    //////////////////////USERS test ok
  test("GET /users returns 200 OK", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.get("/users");

    expect(response.statusCode, 200);
    expect(response, hasBody(isNotNull));
  });

  test("GET /users/9 returns 200 OK", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.get("/users/id=9");

    expect(response.statusCode, 200);
    expect(response, hasBody( {
      "id": 9,
      "username" : "gg",
      "surname" : "gg",
      "lastname" : "gg",
      "address" : "gg",
      "deleted": true,
      "city" : {
          "cityId": 1,
          "city": "MONTPELLIER",
          "zipCode": 34000
      },
      "role": {
          "roleId": 1,
          "role": "user"
      }
    }));
  });  

  test("DELETE /users/9 returns 200 OK", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.delete("/users/id=9");

    expect(response.statusCode, 200);
    expect(response, hasBody(isNotNull));
  });

  test("PUT /users/9 Code 200 et body", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.put("/users/id=9",body: {
      "surname": "gg",
      "lastname": "gg",
      "address": "gg",
      "username": "gg",
      "city": {
          "city": "MONTPELLIER",
          "zipCode": 34000
      }
    });

    expect(response.statusCode, 200);
    final thingId = response.body.as<Map>()["id"];
    final getResponse = await harness.agent.get("/users/id=$thingId");
    expect(getResponse,hasBody( {
      "id": thingId,
      "username" : "gg",
      "surname" : "gg",
      "lastname" : "gg",
      "address" : "gg",
      "deleted": true,
      "city" : {
          "cityId": 1,
          "city": "MONTPELLIER",
          "zipCode": 34000
      },
      "role": {
          "roleId": 1,
          "role": "user"
      }
    }));
  });
    //Dans risque aléatoire de ne pas passé a cause du random user name
    test("POST alexi returns 200 OK", () async {
    var rng = new Random();
    var randVal = rng.nextInt(1000);
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final postResponse = await harness.agent.post("/register", body: {
                                                                      "username" : "lmm@b.fr$randVal",
                                                                      "password" : "lama",
                                                                      "surname" : "Alexi",
                                                                      "lastname" : "ff",
                                                                      "address" : "1fffff",
                                                                      "city" : {
                                                                          "city": "Montpellier",
                                                                          "zipCode": 34000
                                                                      }
                                                                  });
    expect(postResponse.statusCode, 200);
    final thingId = postResponse.body.as<Map>()["id"];
    final getResponse = await harness.agent.get("/users/id=$thingId");
    expect(getResponse,hasBody({
      "id": thingId,
      "username" : "lmm@b.fr$randVal",
      "surname" : "Alexi",
      "lastname" : "ff",
      "address" : "1fffff",
      "deleted": false,
      "city" : {
          "cityId": 1,
          "city": "MONTPELLIER",
          "zipCode": 34000
      },
      "role": {
          "roleId": 1,
          "role": "user"
      }
    }));
  });

  /////////////USER TEST NEGATIF VALIDE INPUT 
  test("POST 409 NOK", () async {
    var rng = new Random();
    var randVal = rng.nextInt(1000);
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final postResponse = await harness.agent.post("/register", body: {
                                                                      "username" : "test2@test.fr",
                                                                      "password" : "lama",
                                                                      "surname" : "Alexi",
                                                                      "lastname" : "Lelardoux",
                                                                      "address" : "183 rue victor Hugo",
                                                                      "city" : {
                                                                          "city": "Montpellier",
                                                                          "zipCode": 34000
                                                                      }
                                                                  });
    expect(postResponse.statusCode, 409);
    expect(postResponse,hasBody({
    "error": "entity_already_exists",
    "detail": "Offending Items: _user.null"
    }));
  });

/////////////USER TEST NEGATIF et NEGATIF INPUT 
test("GET user/789999 NOK", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.get("/users/id=789999");

    expect(response.statusCode, 404);
    expect(response, hasBody(isNull));
  });  

  test("DELETE /users/789999 NOT CONTENT", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.delete("/users/id=789999");

    expect(response.statusCode, 204);
    expect(response, hasBody(isNull));
  });

  test("PUT /users/789999", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.put("/users/id=789999",body: {
      "surname": "gg",
      "lastname": "gg",
      "address": "gg",
      "username": "gg",
      "city": {
          "city": "MONTPELLIER",
          "zipCode": 34000
      }
    });

    expect(response.statusCode, 204);
    expect(response, hasBody(isNull));

  });
  test("PUT /users/9", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.put("/users/id=9",body: {
      "surname": "gg",
      "lastname": "gg",
      "address": "gg",
      "username": "test2@test.fr",
      "city": {
          "city": "MONTPELLIER",
          "zipCode": 34000
      }
    });

    expect(response.statusCode, 409);
    expect(response, hasBody({
    "error": "entity_already_exists",
    "detail": "Offending Items: _user.null"
    }));

  });
    test("POST NOK", () async {
    var rng = new Random();
    var randVal = rng.nextInt(1000);
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final postResponse = await harness.agent.post("/register", body: {
                                                                      "username" : "test2@test.fr",
                                                                      "password" : "lama",
                                                                      "surname" : "Alexi",
                                                                      "lastname" : "ff",
                                                                      "address" : "1fffff",
                                                                      "city" : {
                                                                          "city": "Montpellier",
                                                                          "zipCode": 34000
                                                                      }
                                                                  });
    expect(postResponse.statusCode, 409);
    expect(postResponse, hasBody({
    "error": "entity_already_exists",
    "detail": "Offending Items: _user.null"
    }));
    });

}



void testUsers(harness,token) {
    //////////////////////USERS test ok
  test("GET /users returns 200 OK", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.get("/users");

    expect(response.statusCode, 200);
    expect(response, hasBody(isNotNull));
  });

  test("GET /users/9 returns 200 OK", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.get("/users/id=9");

    expect(response.statusCode, 200);
    expect(response, hasBody( {
      "id": 9,
      "username" : "gg",
      "surname" : "gg",
      "lastname" : "gg",
      "address" : "gg",
      "deleted": true,
      "city" : {
          "cityId": 1,
          "city": "MONTPELLIER",
          "zipCode": 34000
      },
      "role": {
          "roleId": 1,
          "role": "user"
      }
    }));
  });  

  test("DELETE /users/9 returns 200 OK", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.delete("/users/id=9");

    expect(response.statusCode, 200);
    expect(response, hasBody(isNotNull));
  });

  test("PUT /users/9 Code 200 et body", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.put("/users/id=9",body: {
      "surname": "gg",
      "lastname": "gg",
      "address": "gg",
      "username": "gg",
      "city": {
          "city": "MONTPELLIER",
          "zipCode": 34000
      }
    });

    expect(response.statusCode, 200);
    final thingId = response.body.as<Map>()["id"];
    final getResponse = await harness.agent.get("/users/id=$thingId");
    expect(getResponse,hasBody( {
      "id": thingId,
      "username" : "gg",
      "surname" : "gg",
      "lastname" : "gg",
      "address" : "gg",
      "deleted": true,
      "city" : {
          "cityId": 1,
          "city": "MONTPELLIER",
          "zipCode": 34000
      },
      "role": {
          "roleId": 1,
          "role": "user"
      }
    }));
  });
    //Dans risque aléatoire de ne pas passé a cause du random user name
    test("POST alexi returns 200 OK", () async {
    var rng = new Random();
    var randVal = rng.nextInt(1000);
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final postResponse = await harness.agent.post("/register", body: {
                                                                      "username" : "lmm@b.fr$randVal",
                                                                      "password" : "lama",
                                                                      "surname" : "Alexi",
                                                                      "lastname" : "ff",
                                                                      "address" : "1fffff",
                                                                      "city" : {
                                                                          "city": "Montpellier",
                                                                          "zipCode": 34000
                                                                      }
                                                                  });
    expect(postResponse.statusCode, 200);
    final thingId = postResponse.body.as<Map>()["id"];
    final getResponse = await harness.agent.get("/users/id=$thingId");
    expect(getResponse,hasBody({
      "id": thingId,
      "username" : "lmm@b.fr$randVal",
      "surname" : "Alexi",
      "lastname" : "ff",
      "address" : "1fffff",
      "deleted": false,
      "city" : {
          "cityId": 1,
          "city": "MONTPELLIER",
          "zipCode": 34000
      },
      "role": {
          "roleId": 1,
          "role": "user"
      }
    }));
  });

  /////////////USER TEST NEGATIF VALIDE INPUT 
  test("POST 409 NOK", () async {
    var rng = new Random();
    var randVal = rng.nextInt(1000);
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final postResponse = await harness.agent.post("/register", body: {
                                                                      "username" : "test2@test.fr",
                                                                      "password" : "lama",
                                                                      "surname" : "Alexi",
                                                                      "lastname" : "Lelardoux",
                                                                      "address" : "183 rue victor Hugo",
                                                                      "city" : {
                                                                          "city": "Montpellier",
                                                                          "zipCode": 34000
                                                                      }
                                                                  });
    expect(postResponse.statusCode, 409);
    expect(postResponse,hasBody({
    "error": "entity_already_exists",
    "detail": "Offending Items: _user.null"
    }));
  });

/////////////USER TEST NEGATIF et NEGATIF INPUT 
test("GET user/789999 NOK", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.get("/users/789999");

    expect(response.statusCode, 404);
    expect(response, hasBody(isNull));
  });  

  test("DELETE /users/789999 NOT CONTENT", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.delete("/users/789999");

    expect(response.statusCode, 204);
    expect(response, hasBody(isNull));
  });

  test("PUT /users/789999", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.put("/users/789999",body: {
      "surname": "gg",
      "lastname": "gg",
      "address": "gg",
      "username": "gg",
      "city": {
          "city": "MONTPELLIER",
          "zipCode": 34000
      }
    });

    expect(response.statusCode, 204);
    expect(response, hasBody(isNull));

  });
  test("PUT /users/9", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.put("/users/9",body: {
      "surname": "gg",
      "lastname": "gg",
      "address": "gg",
      "username": "test2@test.fr",
      "city": {
          "city": "MONTPELLIER",
          "zipCode": 34000
      }
    });

    expect(response.statusCode, 409);
    expect(response, hasBody({
    "error": "entity_already_exists",
    "detail": "Offending Items: _user.null"
    }));

  });
    test("POST NOK", () async {
    var rng = new Random();
    var randVal = rng.nextInt(1000);
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final postResponse = await harness.agent.post("/register", body: {
                                                                      "username" : "test2@test.fr",
                                                                      "password" : "lama",
                                                                      "surname" : "Alexi",
                                                                      "lastname" : "ff",
                                                                      "address" : "1fffff",
                                                                      "city" : {
                                                                          "city": "Montpellier",
                                                                          "zipCode": 34000
                                                                      }
                                                                  });
    expect(postResponse.statusCode, 409);
    expect(postResponse, hasBody({
    "error": "entity_already_exists",
    "detail": "Offending Items: _user.null"
    }));
    });

}