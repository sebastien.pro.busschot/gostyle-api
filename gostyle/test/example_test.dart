import 'dart:math';

import 'coupon_test.dart';
import 'harness/app.dart';
import 'user_test.dart';

Future main() async {
  const token = "gnS5C7S3Qe2TLqYSOvAa4hkMZLHQpAWW";
  final harness = Harness()
    ..options.configurationFilePath = 'config.yaml'
    ..install();
  testUsers(harness,token);
  testCoupons(harness, token);


}