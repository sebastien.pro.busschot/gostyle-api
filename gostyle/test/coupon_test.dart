import 'dart:math';

import 'harness/app.dart';
Future main() async {
  const token = "1wiK3i64txDxiwB8UwvsJd7e0Hl89VkS";
  final harness = Harness()..install()..options.configurationFilePath = 'config.yaml';
  test("GET coupons returns 200 OK", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.get("/coupons");

    expect(response.statusCode, 200);
    expect(response, hasBody(isNotNull));
  });

  test("GET /coupons/2 returns 200 OK", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.get("/coupons/2");

    expect(response.statusCode, 200);
    expect(response, hasBody( {
      "couponId": 2,
      "coupon": "test-modif",
      "issueDate": "2020-02-11T21:52:00.000Z",
      "expiryDate": "2020-02-12T21:52:00.000Z",
      "percentageDiscount": 10.0,
      "couponDescription": "Un coupon de test",
      "conditions": "une condition",
      "barCode": "123soleil",
      "deleted": true
    }));
  });  
  //Dans risque aléatoire de ne pas passé a cause du random user name
  test("POST COUPON 200 OK", () async {
    var rng = new Random();
    var randVal = rng.nextInt(1000);
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final postResponse = await harness.agent.post("/coupons", body: {
                                                                      "coupon": "test2",
                                                                      "issueDate":"2020-02-11T13:52:00.000Z",
                                                                      "expiryDate":"2020-02-12T13:52:00.000Z",
                                                                      "percentageDiscount": 10.0,
                                                                      "couponDescription":"Un coupon de test2",
                                                                      "conditions":"une condition22",
                                                                      "barCode":"123222222222soleil"
                                                                  });
    expect(postResponse.statusCode, 200);
    final thingId = postResponse.body.as<Map>()["couponId"];
    final getResponse = await harness.agent.get("/coupons/$thingId");
    expect(getResponse,hasBody({
      "couponId": thingId,
      "coupon": "test2",
      "issueDate": "2020-02-11T13:52:00.000Z",
      "expiryDate": "2020-02-12T13:52:00.000Z",
      "percentageDiscount": 10.0,
      "couponDescription":"Un coupon de test2",
      "conditions":"une condition22",
      "barCode":"123222222222soleil",
      "deleted": false

    }));
  });

  test("DELETE /coupons/2 returns 200 OK", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.delete("/coupons/2");

    expect(response.statusCode, 200);
    expect(response, hasBody(isNotNull));
  });

  test("PUT /coupons/2 Code 200 et body", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.put("/coupons/2",body: {
      "coupon": "test-modif",
      "issueDate": "2020-02-11T21:52:00.000Z",
      "expiryDate": "2020-02-12T21:52:00.000Z",
      "percentageDiscount": 10.0,
      "couponDescription": "Un coupon de test",
      "conditions": "une condition",
      "barCode": "123soleil",
      "deleted": true
    });

    expect(response.statusCode, 200);
    final thingId = response.body.as<Map>()["couponId"];
    final getResponse = await harness.agent.get("/coupons/$thingId");
    expect(getResponse,hasBody( {
      "couponId": thingId,
      "coupon": "test-modif",
      "issueDate": "2020-02-11T21:52:00.000Z",
      "expiryDate": "2020-02-12T21:52:00.000Z",
      "percentageDiscount": 10.0,
      "couponDescription": "Un coupon de test",
      "conditions": "une condition",
      "barCode": "123soleil",
      "deleted": true
    }));
  });



/////////////USER TEST NEGATIF et NEGATIF INPUT 
test("GET coupons/789999 NOK", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.get("/coupons/789999");

    expect(response.statusCode, 204);
    expect(response, hasBody(isNull));
  });  

  test("DELETE /coupons/789999 NOT CONTENT", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.delete("/coupons/789999");

    expect(response.statusCode, 204);
    expect(response, hasBody(isNull));
  });

  test("PUT /coupons/789999", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.put("/coupons/789999",body: {
      "coupon": "test-modif",
      "issueDate": "2020-02-11T21:52:00.000Z",
      "expiryDate": "2020-02-12T21:52:00.000Z",
      "percentageDiscount": 10.0,
      "couponDescription": "Un coupon de test",
      "conditions": "une condition",
      "barCode": "123soleil",
      "deleted": true
    });

    expect(response.statusCode, 204);
    expect(response, hasBody(isNull));

  });
  test("PUT /coupons/9", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.put("/coupons/9",body: {
      "surname": "gg",
      "lastname": "gg",
      "address": "gg",
      "username": "test2@test.fr",
      "city": {
          "city": "MONTPELLIER",
          "zipCode": 34000
      }
    });

    expect(response.statusCode, 400);
    expect(response, hasBody(isNotNull));

  });
    test("POST NOK", () async {
    var rng = new Random();
    var randVal = rng.nextInt(1000);
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final postResponse = await harness.agent.post("/coupons", body: {
                                                                      "username" : "test2@test.fr",
                                                                      "password" : "lama",
                                                                      "surname" : "Alexi",
                                                                      "lastname" : "ff",
                                                                      "address" : "1fffff",
                                                                      "city" : {
                                                                          "city": "Montpellier",
                                                                          "zipCode": 34000
                                                                      }
                                                                  });
    expect(postResponse.statusCode, 400);
    expect(postResponse, hasBody(isNotNull));
    });
}
void testCoupons(harness,token) {
    //////////////////////USERS test ok
  test("GET coupons returns 200 OK", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.get("/coupons");

    expect(response.statusCode, 200);
    expect(response, hasBody(isNotNull));
  });

  test("GET /coupons/2 returns 200 OK", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.get("/coupons/2");

    expect(response.statusCode, 200);
    expect(response, hasBody( {
      "couponId": 2,
      "coupon": "test-modif",
      "issueDate": "2020-02-11T21:52:00.000Z",
      "expiryDate": "2020-02-12T21:52:00.000Z",
      "percentageDiscount": 10.0,
      "couponDescription": "Un coupon de test",
      "conditions": "une condition",
      "barCode": "123soleil",
      "deleted": true
    }));
  });  
  //Dans risque aléatoire de ne pas passé a cause du random user name
  test("POST COUPON 200 OK", () async {
    var rng = new Random();
    var randVal = rng.nextInt(1000);
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final postResponse = await harness.agent.post("/coupons", body: {
                                                                      "coupon": "test2",
                                                                      "issueDate":"2020-02-11T13:52:00.000Z",
                                                                      "expiryDate":"2020-02-12T13:52:00.000Z",
                                                                      "percentageDiscount": 10.0,
                                                                      "couponDescription":"Un coupon de test2",
                                                                      "conditions":"une condition22",
                                                                      "barCode":"123222222222soleil"
                                                                  });
    expect(postResponse.statusCode, 200);
    final thingId = postResponse.body.as<Map>()["couponId"];
    final getResponse = await harness.agent.get("/coupons/$thingId");
    expect(getResponse,hasBody({
      "couponId": thingId,
      "coupon": "test2",
      "issueDate": "2020-02-11T13:52:00.000Z",
      "expiryDate": "2020-02-12T13:52:00.000Z",
      "percentageDiscount": 10.0,
      "couponDescription":"Un coupon de test2",
      "conditions":"une condition22",
      "barCode":"123222222222soleil",
      "deleted": false

    }));
  });

  test("DELETE /coupons/2 returns 200 OK", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.delete("/coupons/2");

    expect(response.statusCode, 200);
    expect(response, hasBody(isNotNull));
  });

  test("PUT /coupons/2 Code 200 et body", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.put("/coupons/2",body: {
      "coupon": "test-modif",
      "issueDate": "2020-02-11T21:52:00.000Z",
      "expiryDate": "2020-02-12T21:52:00.000Z",
      "percentageDiscount": 10.0,
      "couponDescription": "Un coupon de test",
      "conditions": "une condition",
      "barCode": "123soleil",
      "deleted": true
    });

    expect(response.statusCode, 200);
    final thingId = response.body.as<Map>()["couponId"];
    final getResponse = await harness.agent.get("/coupons/$thingId");
    expect(getResponse,hasBody( {
      "couponId": thingId,
      "coupon": "test-modif",
      "issueDate": "2020-02-11T21:52:00.000Z",
      "expiryDate": "2020-02-12T21:52:00.000Z",
      "percentageDiscount": 10.0,
      "couponDescription": "Un coupon de test",
      "conditions": "une condition",
      "barCode": "123soleil",
      "deleted": true
    }));
  });



/////////////USER TEST NEGATIF et NEGATIF INPUT 
test("GET coupons/789999 NOK", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.get("/coupons/789999");

    expect(response.statusCode, 204);
    expect(response, hasBody(isNull));
  });  

  test("DELETE /coupons/789999 NOT CONTENT", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.delete("/coupons/789999");

    expect(response.statusCode, 204);
    expect(response, hasBody(isNull));
  });

  test("PUT /coupons/789999", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.put("/coupons/789999",body: {
      "coupon": "test-modif",
      "issueDate": "2020-02-11T21:52:00.000Z",
      "expiryDate": "2020-02-12T21:52:00.000Z",
      "percentageDiscount": 10.0,
      "couponDescription": "Un coupon de test",
      "conditions": "une condition",
      "barCode": "123soleil",
      "deleted": true
    });

    expect(response.statusCode, 204);
    expect(response, hasBody(isNull));

  });
  test("PUT /coupons/9", () async {
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final response = await harness.agent.put("/coupons/9",body: {
      "surname": "gg",
      "lastname": "gg",
      "address": "gg",
      "username": "test2@test.fr",
      "city": {
          "city": "MONTPELLIER",
          "zipCode": 34000
      }
    });

    expect(response.statusCode, 400);
    expect(response, hasBody(isNotNull));

  });
    test("POST NOK", () async {
    var rng = new Random();
    var randVal = rng.nextInt(1000);
    harness.agent.headers[HttpHeaders.authorizationHeader] = "Bearer $token";
    final postResponse = await harness.agent.post("/coupons", body: {
                                                                      "username" : "test2@test.fr",
                                                                      "password" : "lama",
                                                                      "surname" : "Alexi",
                                                                      "lastname" : "ff",
                                                                      "address" : "1fffff",
                                                                      "city" : {
                                                                          "city": "Montpellier",
                                                                          "zipCode": 34000
                                                                      }
                                                                  });
    expect(postResponse.statusCode, 400);
    expect(postResponse, hasBody(isNotNull));
    });
}