class Authorizations {
  static Map<String, String> cityAuth = {'GET': 'true', 'GETONE': 'true','POST': 'false', 'DELETE': 'false', 'PUT': 'false'};
  static Map<String, String> couponAuth = {'GET': 'true', 'GETONE': 'true','POST': 'false', 'DELETE': 'false', 'PUT': 'false'};
  static Map<String, String> couponProductAuth = {'GET': 'true', 'GETONE': 'true','POST': 'false', 'DELETE': 'false', 'PUT': 'false'};
  static Map<String, String> couponCartAuth = {'GET': 'true', 'GETONE': 'true','POST': 'true', 'DELETE': 'true', 'PUT': 'true'};
  static Map<String, String> productAuth = {'GET': 'true', 'GETONE': 'true','POST': 'false', 'DELETE': 'false', 'PUT': 'false'};
  static Map<String, String> qrcodeAuth = {'GET': 'true', 'GETONE': 'true','POST': 'false', 'DELETE': 'false', 'PUT': 'false'};
  static Map<String, String> roleAuth = {'GET': 'true', 'GETONE': 'true','POST': 'false', 'DELETE': 'false', 'PUT': 'false'};
  static Map<String, String> userAuth = {'GET': 'false', 'GETONE': 'true','POST': 'false', 'DELETE': 'check', 'PUT': 'check'};


  static String canAccess(String resource, String method) {
    String answer = 'false';
    switch(resource) {
      case 'cities':
        answer = cityAuth[method];
        break;
      case 'coupons':
        answer = couponAuth[method];
        break;
      case 'couponsProducts':
        answer = couponProductAuth[method];
        break;
      case 'couponsCart':
        answer = couponCartAuth[method];
        break;
      case 'products':
        answer = productAuth[method];
        break;
      case 'qrcodes':
        answer = qrcodeAuth[method];
        break;
      case 'roles':
        answer = roleAuth[method];
        break;
      case 'users':
        answer = userAuth[method];
        break;
    }

    return answer;
  }

}