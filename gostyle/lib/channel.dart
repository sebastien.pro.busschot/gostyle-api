import 'package:aqueduct/managed_auth.dart';
import 'package:gostyle/controller/c_cities.dart';
import 'package:gostyle/controller/c_couponsCart.dart';
import 'package:gostyle/controller/c_coupons_products.dart';
import 'package:gostyle/controller/c_files.dart';
import 'package:gostyle/controller/c_products.dart';
import 'package:gostyle/controller/c_qrcodes.dart';
import 'package:gostyle/controller/c_register.dart';
import 'package:gostyle/controller/c_users.dart';
import 'package:gostyle/model/user.dart';
import 'package:gostyle/gostyle_configuration.dart';
import 'package:gostyle/gostyle.dart';
import 'package:gostyle/role_filter.dart';

import 'controller/c_coupons.dart';
import 'controller/c_roles.dart';

class GostyleChannel extends ApplicationChannel {
  final fileDir = "uploads";
  ManagedContext context;

  AuthServer authServer;

  @override
  Future prepare() async {
    final config = GostyleConfiguration(options.configurationFilePath);
    final dataModel = ManagedDataModel.fromCurrentMirrorSystem();
    final psc = PostgreSQLPersistentStore.fromConnectionInfo(
        config.database.username,
        config.database.password,
        config.database.host,
        config.database.port,
        config.database.databaseName);

    context = ManagedContext(dataModel, psc);

    final authStorage = ManagedAuthDelegate<User>(context);
    authServer = AuthServer(authStorage);


    logger.onRecord.listen((rec) => print("$rec ${rec.error ?? ""} ${rec.stackTrace ?? ""}"));
  }

  @override
  Controller get entryPoint {
    final router = Router();

    router
        .route('/auth/token')
        .link(() => AuthController(authServer));
    router
        .route('/register')
        .link(() => RegisterController(context, authServer));
    router
        .route("/users/[:params]")
        .link(() => Authorizer.bearer(authServer))
        .link(() => RoleFilter(context))
        .link(() => UsersController(context, authServer));

    router
        .route("/cities/[:id]")
        .link(() => CitiesController(context));

    router
        .route("/roles/[:id]")
        .link(() => Authorizer.bearer(authServer))
        .link(() => RoleFilter(context))
        .link(() => RolesController(context));

    router
        .route("/coupons/[:id]")
        .link(() => Authorizer.bearer(authServer))
        .link(() => RoleFilter(context))
        .link(() => CouponController(context));

    router
        .route("/products/[:id]")
        .link(() => Authorizer.bearer(authServer))
        .link(() => RoleFilter(context))
        .link(() => ProductController(context));

    router
        .route("/qrcodes/[:id]")
        .link(() => Authorizer.bearer(authServer))
        .link(() => RoleFilter(context))
        .link(() => QRCodeController(context));
    router
        .route("/couponsProducts/[:params]")
        .link(() => Authorizer.bearer(authServer))
        .link(() => RoleFilter(context))
        .link(() => CouponsProductsController(context));
    router
        .route("/couponsCart/[:id]")
        .link(() => Authorizer.bearer(authServer))
        .link(() => RoleFilter(context))
        .link(() => CouponsCartController(context));
    router
        .route("/files/[:id]")
        .link(() => Authorizer.bearer(authServer))
        .link(() => FilesController(context, fileDir));

    router
        .route("files/show/*")
        .link(() => FileController(fileDir));

    return router;
  }
}
