import 'package:aqueduct/aqueduct.dart';
import 'package:gostyle/gostyle.dart';
import 'package:gostyle/model/user.dart';
import 'package:gostyle/controller/c_register.dart';

class UsersController extends ResourceController {
  UsersController(this.context, this.authServer);

  final ManagedContext context;
  final AuthServer authServer;

  @Operation.get()
  Future<Response> getAllUsers() async {
    final userQuery = Query<User>(context)..join(object: (u) => u.city)..join(object: (u) => u.role);
    return Response.ok(await userQuery.fetch());
  }

  @Operation.get('params')
  Future<Response> getUserByID(@Bind.path('params') String params) async {
    final key = params.split("=")[1];
    final item = params.split("=")[0];

    final query = Query<User>(context);

    switch(item) {
      case 'id':
        query
          ..where((user) => user.id).equalTo(int.parse(key))..join(object: (u) => u.city)..join(object: (u) => u.role);
        break;
      case 'username':
        query
          ..where((user) => user.username).equalTo(key)..join(object: (u) => u.city)..join(object: (u) => u.role);
        break;
      default:
        query
          ..where((user) => user.id).equalTo(int.parse(key))..join(object: (u) => u.city)..join(object: (u) => u.role);
        break;
    }

    final user = await query.fetchOne();

    return user == null ? Response.notFound() : Response.ok(user);
  }

  @Operation.put('params')
  Future<Response> updateUser(@Bind.path('params') String params, @Bind.body() User user) async {
    final key = int.parse(params.split("=")[1]);
    user.city.city = user.city.city.toUpperCase();

    final userQuery = Query<User>(context)
      ..values.surname = user.surname
      ..values.lastname = user.lastname
      ..values.address = user.address
      ..values.username = user.username
      ..values.city = await RegisterController.checkOrUpdateCity(user.city, context)
      ..where((user) => user.id).equalTo(key);
    final User updatedUser = await userQuery.updateOne();

    if(updatedUser == null) {
      return Response.noContent();
    }

    return Response.ok(updatedUser);
  }

  @Operation.delete('params')
  Future<Response> deleteUser(@Bind.path('params') String params) async {
    final key = int.parse(params.split("=")[1]);

    final userQuery = Query<User>(context)
      ..values.deleted = true
      ..where((user) => user.id).equalTo(key);
    final updatedUser = await userQuery.updateOne();

    if(updatedUser == null) {
      return Response.noContent();
    }

    return Response.ok(updatedUser);
  }

}