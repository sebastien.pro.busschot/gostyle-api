import 'package:aqueduct/aqueduct.dart';
import 'package:gostyle/gostyle.dart';
import 'package:gostyle/model/couponCart.dart';
import 'package:gostyle/model/user.dart';

class CouponsCartController extends ResourceController {

  CouponsCartController(this.context);
  final ManagedContext context;

  @Operation.post()
  Future<Response> create(@Bind.body() CouponCart couponCart) async {
    final query = Query<CouponCart>(context)
      ..values = couponCart;
    final result = await query.insert();

    return Response.ok(result);
  }

  @Operation.delete('id')
  Future<Response> delete(@Bind.path('id') int id) async {
    final query = Query<CouponCart>(context)
      ..where((model) => model.couponCartId).equalTo(id);

    final int deleted = await query.delete();

    if(deleted == 0){
      return Response.noContent();
    }

    return Response.ok({
      "status" : 200,
      "deleted" : true
    });
  }



  @Operation.get('id')
  Future<Response> retrieve(@Bind.path('id') int id) async {
    final query = Query<CouponCart>(context)..where((model)=>model.couponCartId).equalTo(id)
      ..join(object: (c) => c.user)
          .join(object: (u) => u.city)
      ..join(object: (c) => c.coupon);

    final subject = await query.fetchOne();

    if(subject == null){
      return Response.noContent();
    }

    return Response.ok(subject);
  }

  @Operation.get()
  Future<Response> getAll() async {
    List<CouponCart> rows;

    final query = Query<User>(context)
      ..join(object: (u) => u.role)
      ..where((model)=>model.id).equalTo(request.authorization.ownerID);
    final User user = await query.fetchOne();

    if(user.role.role != 'admin') {
      final query = Query<CouponCart>(context)
        ..join(object: (c) => c.user)
            .join(object: (u) => u.city)
        ..join(object: (c) => c.coupon)
        ..where((u) => u.user.id).equalTo(request.authorization.ownerID);
      rows = await query.fetch();
    }
    else {
      final query = Query<CouponCart>(context)
        ..join(object: (c) => c.user)
            .join(object: (u) => u.city)
        ..join(object: (c) => c.coupon);
      rows = await query.fetch();
    }

    return Response.ok(rows);
  }

  static Future<dynamic> insert(ManagedContext context, CouponCart couponCart) async {
    final query = Query<CouponCart>(context)
      ..values = couponCart;

    return await query.insert();
  }


}