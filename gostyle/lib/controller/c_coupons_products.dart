import 'package:aqueduct/aqueduct.dart';
import 'package:gostyle/gostyle.dart';
import 'package:gostyle/model/couponProduct.dart';

class CouponsProductsController extends ResourceController {

  CouponsProductsController(this.context);
  final ManagedContext context;

  @Operation.post()
  Future<Response> create(@Bind.body() CouponProduct couponProduct) async {
    final query = Query<CouponProduct>(context)
      ..values = couponProduct;
    final result = await query.insert();

    return Response.ok(result);
  }

  @Operation.delete('id')
  Future<Response> delete(@Bind.path('id') int id) async {
    final query = Query<CouponProduct>(context)
      ..where((model) => model.couponProductId).equalTo(id);

    final int deleted = await query.delete();

    if(deleted == 0){
      return Response.noContent();
    }

    return Response.ok({
      "status" : 200,
      "deleted" : true
    });
  }



  @Operation.get('params')
  Future<Response> retrieve(@Bind.path('params') String params) async {
    final id = int.parse(params.split("=")[1]);
    final item = params.split("=")[0];

    final query = Query<CouponProduct>(context);

    switch(item) {
      case 'coupon':
        query
          ..where((model) => model.coupon.couponId).equalTo(id)
          ..join(object: (u) => u.product).join(object: (u) => u.file)
          ..join(object: (u) => u.coupon);
        break;
      case 'product':
        query
          ..where((model) => model.product.productId).equalTo(id)
          ..join(object: (u) => u.product).join(object: (u) => u.file)
          ..join(object: (u) => u.coupon);
        break;
      default:
        query
          ..where((model) => model.couponProductId).equalTo(id)
          ..join(object: (u) => u.product).join(object: (u) => u.file)
          ..join(object: (u) => u.coupon);
        break;
    }

    final List<CouponProduct> rows = await query.fetch();

    if(rows == null){
      return Response.noContent();
    }

    return Response.ok(rows);
  }

  @Operation.get()
  Future<Response> getAll() async {
    final query = Query<CouponProduct>(context)
      ..join(object: (u) => u.product).join(object: (u) => u.file)
      ..join(object: (u) => u.coupon);
    final List<CouponProduct> rows = await query.fetch();

    return Response.ok(rows);
  }


}