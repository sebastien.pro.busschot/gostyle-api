import 'package:aqueduct/aqueduct.dart';
import 'package:gostyle/gostyle.dart';
import 'package:gostyle/model/role.dart';

class RolesController extends ResourceController {
  RolesController(this.context);
  final ManagedContext context;


  @Operation.get()
  Future<Response> getAllRoles() async {
    final roleQuery = Query<Role>(context);
    return Response.ok(await roleQuery.fetch());
  }

  @Operation.get('id')
  Future<Response> getRoleByID(@Bind.path('id') int id) async {
    final roleQuery = Query<Role>(context)..where((role) => role.roleId).equalTo(id);
    final role = await roleQuery.fetchOne();

    return role == null ? Response.notFound() : Response.ok(role);
  }

}