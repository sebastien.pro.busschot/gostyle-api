

import 'package:aqueduct/aqueduct.dart';
import 'package:gostyle/gostyle.dart';
import 'package:gostyle/model/coupon.dart';

class CouponController extends ResourceController {

  CouponController(this.context);
  final ManagedContext context;

  @Operation.post()
  Future<Response> create(@Bind.body() Coupon coupon) async {
    final query = Query<Coupon>(context)
      ..values = coupon;
    final result = await query.insert();

    return Response.ok(result);
  }

  @Operation.delete('id')
  Future<Response> delete(@Bind.path('id') int id) async {
    final query = Query<Coupon>(context)
      ..values.deleted = true
      ..where((coupon) => coupon.couponId).equalTo(id);
    final updated = await query.updateOne();

    if(updated == null) {
      return Response.noContent();
    }

    return Response.ok(updated);
  }

  @Operation.put('id')
  Future<Response> update(@Bind.path('id') int id, @Bind.body() Coupon coupon) async {
    final query = Query<Coupon>(context)
      ..values.coupon = coupon.coupon
      ..values.issueDate = coupon.issueDate
      ..values.expiryDate = coupon.expiryDate
      ..values.percentageDiscount = coupon.percentageDiscount
      ..values.couponDescription = coupon.couponDescription
      ..values.conditions = coupon.conditions
      ..values.barCode = coupon.barCode
      ..values.deleted = coupon.deleted
      ..where((model) => model.couponId).equalTo(id);

    final updated = await query.updateOne();

    if(updated == null){
      return Response.noContent();
    }

    return Response.ok(updated);
  }

  @Operation.get('id')
  Future<Response> retrieve(@Bind.path('id') int id) async {
    final query = Query<Coupon>(context)..where((model)=>model.couponId).equalTo(id);

    final subject = await query.fetchOne();

    if(subject == null){
      return Response.noContent();
    }

    return Response.ok(subject);
  }

  @Operation.get()
  Future<Response> getAll() async {
    final query = Query<Coupon>(context);
    final List<Coupon> rows = await query.fetch();

    return Response.ok(rows);
  }

  static Future<dynamic> insert(ManagedContext context, Coupon coupon) async {
    final query = Query<Coupon>(context)
      ..values = coupon;

    return await query.insert();
  }


}