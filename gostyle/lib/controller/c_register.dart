import 'package:gostyle/model/role.dart';
import 'package:gostyle/model/user.dart';
import 'package:gostyle/model/city.dart';

import '../gostyle.dart';

class RegisterController extends ResourceController {
  RegisterController(this.context, this.authServer);

  final ManagedContext context;
  final AuthServer authServer;


  @Operation.post()
  Future<Response> addUser(@Bind.body() User user) async {
    user.city.city = user.city.city.toUpperCase();
    user.city = await checkOrUpdateCity(user.city, context);

    user.role = await getUserRole();

    if (user.username == null || user.password == null) {
      return Response.badRequest(
          body: {"error": "username and password required."});
    }

    user
      ..salt = AuthUtility.generateRandomSalt()
      ..hashedPassword = authServer.hashPassword(user.password, user.salt);

    return Response.ok(await Query(context, values: user).insert());
  }


  Future<Role> getUserRole() async {
    final roleQuery = Query<Role>(context)
        ..where((role) => role.role).equalTo("user");
    final role = await roleQuery.fetchOne();

    return role;
  }



  static Future<City> checkOrUpdateCity(City oldCity, ManagedContext context) async{
    City city;

    final cityQuery = Query<City>(context)
      ..where((city) => city.city).equalTo(oldCity.city)
      ..where((city) => city.zipCode).equalTo(oldCity.zipCode);

    city = await cityQuery.fetchOne();

    if(city == null) {
      final cityAddQuery = Query<City>(context)
        ..values.city = oldCity.city
        ..values.zipCode = oldCity.zipCode;
      city = await cityAddQuery.insert();
    }

    return city;
  }

}