import 'package:aqueduct/aqueduct.dart';
import 'package:gostyle/gostyle.dart';
import 'package:gostyle/model/qrcode.dart';

class QRCodeController extends ResourceController {

  QRCodeController(this.context);
  final ManagedContext context;

  @Operation.post()
  Future<Response> create(@Bind.body() QRCode qrcode) async {
    final query = Query<QRCode>(context)
      ..values = qrcode;
    final result = await query.insert();

    return Response.ok(result);
  }

  @Operation.delete('id')
  Future<Response> delete(@Bind.path('id') int id) async {
    final query = Query<QRCode>(context)
      ..where((model) => model.qrCodeId).equalTo(id);

    final int deleted = await query.delete();

    if(deleted == 0){
      return Response.noContent();
    }

    return Response.ok({
      "status" : 200,
      "deleted" : true
    });
  }


  @Operation.get('id')
  Future<Response> retrieve(@Bind.path('id') int id) async {
    final query = Query<QRCode>(context)..where((model)=>model.qrCodeId).equalTo(id)
    ..join(object: (c) => c.coupon);

    final subject = await query.fetchOne();

    if(subject == null){
      return Response.noContent();
    }

    return Response.ok(subject);
  }

  @Operation.get()
  Future<Response> getAll() async {
    final query = Query<QRCode>(context)
      ..join(object: (c) => c.coupon);
    final List<QRCode> rows = await query.fetch();

    return Response.ok(rows);
  }


}