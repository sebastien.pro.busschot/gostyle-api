import 'dart:async';
import 'dart:io';
import 'package:aqueduct/aqueduct.dart';
import 'package:http_server/http_server.dart';
import 'package:gostyle/model/productFile.dart';
import 'package:mime/mime.dart';

class FilesController extends ResourceController {

  FilesController(this.context, this.directoryPath) {
    acceptedContentTypes = [
      ContentType("multipart", "form-data"),
      ContentType("application", "json")
    ];
  }

  ManagedContext context;
  String directoryPath;

  @Operation.post()
  Future<Response> postForm() async {

    final boundary = request.raw.headers.contentType.parameters["boundary"];
    final transformer = MimeMultipartTransformer(boundary);
    final bodyBytes = await request.body.decode<List<int>>();
    final bodyStream = Stream.fromIterable([bodyBytes]);
    final parts = await transformer.bind(bodyStream).toList();
    final files = [];

    for (var part in parts) {
      final Map<String, String> headers = part.headers;
      final HttpMultipartFormData multipart = HttpMultipartFormData.parse(part);
      final content = multipart.cast<List<int>>();
      if (content == null) {
        print(content);
        break;
      }

      final fileName = "${DateTime.now().millisecondsSinceEpoch}-${getFileName(headers)}";
      if (fileName == null) {
        return Response.badRequest(body: {"message":"The header doesn't contain filename"});
      }

      final query = Query<ProductFile>(context)
        ..values.fileName = fileName;

      final filePath = "${directoryPath}/${fileName}";
      await File(filePath).create(recursive: true);
      final IOSink sink = File(filePath).openWrite();

      await content.forEach(sink.add);
      await sink.flush();

      final ProductFile file = await query.insert();
      files.add(file.asMap());

      await sink.close();
    }

    if (files.isEmpty) {
      return Response.badRequest(body: {
        "message" : "no file detected"
      });
    }

    return Response.ok({
      "message" : "file uploaded",
      "files" : files
    });
  }

  @Operation.get('id')
  Future<Response> getById(@Bind.path('id') int id) async {
    final query = Query<ProductFile>(context)
      ..join(object: (m) => m.product)
      ..where((m) => m.fileId).equalTo(id);

    final ProductFile file = await query.fetchOne();
    return Response.ok(file);
  }

  @Operation.get()
  Future<Response> getAll() async {
    final query = Query<ProductFile>(context)
      ..join(object: (m) => m.product);

    final List<ProductFile> files = await query.fetch();
    return Response.ok(files);
  }

  String getFileName(Map<String, String> headers) {
    final List<String> headersSplit = headers["content-disposition"].split(";");

    for ( String split in headersSplit) {
      if ( split.contains("filename") ) {
        String fileName = split.replaceAll(" ", "");
        fileName = fileName.replaceAll("\"", "");
        fileName = fileName.split("=")[1];
        return fileName;
      }
    }

    return null;
  }

  static Future<Response> deleteFile(ManagedContext context, String directoryPath, int id) async {
    final getFile = Query<ProductFile>(context)
      ..where((m) => m.fileId).equalTo(id);

    final ProductFile file = await getFile.fetchOne();

    if (file == null){
      return Response.noContent();
    }

    if(await getFile.delete() > 0){
      final File fileToDelete = File('${directoryPath}/${file.fileName}');
      await fileToDelete.delete();
    }

    return Response.ok({
      'message' : 'deleted from disk and database'
    });
  }
}

