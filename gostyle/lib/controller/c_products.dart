
import 'package:aqueduct/aqueduct.dart';
import 'package:gostyle/gostyle.dart';
import 'package:gostyle/model/product.dart';

class ProductController extends ResourceController {

  ProductController(this.context);
  final ManagedContext context;

  @Operation.post()
  Future<Response> create(@Bind.body() Product product) async {
    final query = Query<Product>(context)
      ..values = product;
    final result = await query.insert();

    return Response.ok(result);
  }

  @Operation.delete('id')
  Future<Response> delete(@Bind.path('id') int id) async {
    final query = Query<Product>(context)
      ..values.deleted = true
      ..where((product) => product.productId).equalTo(id);
    final updated = await query.updateOne();

    if(updated == null) {
      return Response.noContent();
    }

    return Response.ok(updated);
  }

  @Operation.put('id')
  Future<Response> update(@Bind.path('id') int id, @Bind.body() Product product) async {
    final query = Query<Product>(context)
      ..values.product = product.product
      ..values.description = product.description
      ..values.quantity = product.quantity
      ..values.unitPrice = product.unitPrice
      ..values.siteUrl = product.siteUrl
      ..values.deleted = product.deleted
      ..values.file = product.file
      ..where((model) => model.productId).equalTo(id);

    final updated = await query.updateOne();

    if(updated == null){
      return Response.noContent();
    }

    return Response.ok(updated);
  }

  @Operation.get('id')
  Future<Response> retrieve(@Bind.path('id') int id) async {
    final query = Query<Product>(context)
      ..join(object: (m) => m.file)
      ..where((model)=>model.productId).equalTo(id);

    final subject = await query.fetchOne();

    if(subject == null){
      return Response.noContent();
    }

    return Response.ok(subject);
  }

  @Operation.get()
  Future<Response> getAll() async {
    final query = Query<Product>(context)
      ..join(object: (m) => m.file);
    final List<Product> rows = await query.fetch();

    return Response.ok(rows);
  }


}
