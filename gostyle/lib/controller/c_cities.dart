import 'package:aqueduct/aqueduct.dart';
import 'package:gostyle/gostyle.dart';
import 'package:gostyle/model/city.dart';

class CitiesController extends ResourceController {
  CitiesController(this.context);
  final ManagedContext context;


  @Operation.get()
  Future<Response> getAllCities() async {
    final cityQuery = Query<City>(context);
    return Response.ok(await cityQuery.fetch());
  }

  @Operation.get('id')
  Future<Response> getCityByID(@Bind.path('id') int id) async {
    final cityQuery = Query<City>(context)..where((city) => city.cityId).equalTo(id);
    final city = await cityQuery.fetchOne();

    return city == null ? Response.notFound() : Response.ok(city);
  }

  @Operation.post()
  Future<Response> addCity(@Bind.body() City city) async {
    city.city = city.city.toUpperCase();

    final cityQuery = Query<City>(context)..values = city;
    final insertedCity = await cityQuery.insert();

    return Response.ok(insertedCity);
  }

}