import 'gostyle.dart';

class GostyleConfiguration extends Configuration {
  GostyleConfiguration(String configPath) : super.fromFile(File(configPath));

  DatabaseConfiguration database;
}