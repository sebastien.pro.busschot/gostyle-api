import 'package:gostyle/gostyle.dart';
import 'package:gostyle/model/coupon.dart';

class QRCode extends ManagedObject<_QRCode> implements _QRCode {}

class _QRCode {
  @primaryKey
  int qrCodeId;

  @Column()
  String content;

  @Relate(#qrCode)
  Coupon coupon;
}
