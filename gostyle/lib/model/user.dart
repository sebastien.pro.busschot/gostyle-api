import 'package:aqueduct/managed_auth.dart';
import 'package:gostyle/gostyle.dart';
import 'package:gostyle/model/role.dart';
import 'package:gostyle/model/city.dart';
import 'package:gostyle/model/couponCart.dart';

class User extends ManagedObject<_User> implements _User, ManagedAuthResourceOwner<_User> {
  @Serialize(input: true, output: false)
  String password;
}

class _User extends ResourceOwnerTableDefinition {
  @Column()
  String surname;

  @Column()
  String lastname;

  @Column()
  String address;

  @Column(nullable: true, defaultValue: 'false')
  bool deleted;

  @Relate(#users)
  City city;

  @Relate(#users)
  Role role;

  ManagedSet<CouponCart> couponCart;
}