import 'package:gostyle/gostyle.dart';
import 'package:gostyle/model/user.dart';

class City extends ManagedObject<_City> implements _City {}

class _City {
  @primaryKey
  int cityId;

  @Column()
  int zipCode;

  @Column()
  String city;

  ManagedSet<User> users;
}