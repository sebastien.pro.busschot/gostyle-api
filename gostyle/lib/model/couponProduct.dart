import 'package:gostyle/gostyle.dart';
import 'package:gostyle/model/coupon.dart';
import 'package:gostyle/model/product.dart';

class CouponProduct extends ManagedObject<_CouponProduct> implements _CouponProduct {}

class _CouponProduct {
  @primaryKey
  int couponProductId;

  @Relate(#couponProduct)
  Product product;

  @Relate(#couponProduct)
  Coupon coupon;
}
