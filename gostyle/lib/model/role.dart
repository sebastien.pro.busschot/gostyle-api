import 'package:gostyle/gostyle.dart';
import 'package:gostyle/model/user.dart';

class Role extends ManagedObject<_Role> implements _Role {}

class _Role {
  @primaryKey
  int roleId;

  @Column()
  String role;

  ManagedSet<User> users;
}