import 'package:gostyle/gostyle.dart';
import 'package:gostyle/model/coupon.dart';
import 'package:gostyle/model/user.dart';

class CouponCart extends ManagedObject<_CouponCart> implements _CouponCart {}

class _CouponCart {
  @primaryKey
  int couponCartId;

  @Relate(#couponCart)
  User user;

  @Relate(#couponCart)
  Coupon coupon;
}
