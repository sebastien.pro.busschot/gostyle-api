import 'package:gostyle/gostyle.dart';
import 'package:gostyle/model/product.dart';

class ProductFile extends ManagedObject<_ProductFile> implements _ProductFile {}

class _ProductFile {
  @primaryKey
  int fileId;

  @Column(unique: true)
  String fileName;

  Product product;
}