
import 'package:gostyle/gostyle.dart';
import 'package:gostyle/model/couponProduct.dart';

import 'productFile.dart';

class Product extends ManagedObject<_Product> implements _Product {}

class _Product {
  @primaryKey
  int productId;

  @Column()
  String product;
  
  @Column()
  String description;

  @Column()
  int quantity;

  @Column()
  int unitPrice;

  @Column()
  String siteUrl;

  @Column(nullable: true, defaultValue: 'false')
  bool deleted;

  ManagedSet<CouponProduct> couponProduct;

  @Relate(#product)
  ProductFile file;
}
