import 'package:gostyle/gostyle.dart';
import 'package:gostyle/model/qrcode.dart';

import 'couponCart.dart';
import 'couponProduct.dart';

class Coupon extends ManagedObject<_Coupon> implements _Coupon {}

class _Coupon {
  @primaryKey
  int couponId;

  @Column()
  String coupon;
  
  @Column()
  DateTime issueDate;

  @Column()
  DateTime expiryDate;

  @Column(nullable: true)
  double percentageDiscount;

  @Column()
  String couponDescription;

  @Column()
  String conditions;

  @Column()
  String barCode;

  @Column(nullable: true, defaultValue: 'false')
  bool deleted;

  ManagedSet<CouponProduct> couponProduct;
  ManagedSet<CouponCart> couponCart;

  QRCode qrCode;
}
