

import 'authorizations.dart';
import 'gostyle.dart';
import 'model/user.dart';

class RoleFilter extends Controller implements Recyclable {
  RoleFilter(this.context);

  ManagedContext context;

  @override
  Future<RequestOrResponse> handle(Request request) async {
    final User currentUser = await getUser(request);
    final String resourceName = request.path.segments[0];
    String method = request.method;

    if(method == "GET" && (request.path.variables.containsKey('id')
        || request.path.variables.containsKey('params') )) {
      method += "ONE";
    }
    if(request.path.variables.containsKey('id') && resourceName == 'users') {
      if(request.path.variables['id'] == '0') {
        request.path.variables['id'] = currentUser.id.toString();
      }
    }

    if(currentUser.role.role == 'admin') {
        return request;
    }
    else {
      switch(Authorizations.canAccess(resourceName, method)) {
        case 'false':
          return Response.forbidden();
        case 'check':
          if(request.path.variables.containsKey('id')) {
            if (request.path.variables['id'] == currentUser.id.toString()) {
              return request;
            }
            else {
              return Response.forbidden();
            }
          }
          else {
            return Response.forbidden();
          }
          break;
        case 'true':
          return request;
      }

      return Response.forbidden();
    }
  }



  Future<User> getUser(Request request) async {
    final query = Query<User>(context)
      ..join(object: (u) => u.role)
      ..where((model)=>model.id).equalTo(request.authorization.ownerID);
    return await query.fetchOne();
  }

  @override
  // TODO: implement recycledState
  get recycledState => null;

  @override
  void restore(state) {
    // TODO: implement restore
  }
}
