# gostyle

## Versions
`Dart : 2.7.2`
`PostgreSQL : 13.1`
`Aqueduct : 3.3.0+1`

## Install
Create user and launch Postgre
```
# create postgre user
sudo -i -u postgres 

# launch postgresql
psql
```

Create Database and manage user access
```
CREATE DATABASE gostyle;
CREATE USER gostyleuser WITH PASSWORD 'gostylettes';
GRANT ALL ON DATABASE gostyle TO gostyleuser;
```

Integration in the aqueduct project
```
pub get
aqueduct db upgrade
aqueduct serve
```

Get the code64 transcription of the client String
```
dart getClientCredential.dart yourClientString
```


Put the above base 64 string in the headers of your request
```
Authorization : Basic base64ClientString
```

In the aqueduct project, execute the following :
WARNING : do not put the base 64 string
```
aqueduct auth add-client --id yourClientString
```

## Running the Application Locally

Run `aqueduct serve` from this directory to run the application. For running within an IDE, run `bin/main.dart`. By default, a configuration file named `config.yaml` will be used.

To generate a SwaggerUI client, run `aqueduct document client`.

## Running Application Tests

To run all tests for this application, run the following in this directory:

```
pub run test
```

The default configuration file used when testing is `config.src.yaml`. This file should be checked into version control. It also the template for configuration files used in deployment.

## Deploying an Application

See the documentation for [Deployment](https://aqueduct.io/docs/deploy/).